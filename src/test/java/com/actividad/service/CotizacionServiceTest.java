package com.actividad.service;

import com.actividad.cotizacionweb.service.CotizacionService;
import com.actividad.cotizacionweb.repository.CoinDeskCotizacionRepository;
import com.actividad.cotizacionweb.repository.CotizacionRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class CotizacionServiceTest {

    public CotizacionServiceTest() {
    }

    @Test(expected = NullPointerException.class)
    public void obtenerListadoCotizacion_ArgumentoArrayCotizacionRepositoryNull_NullPointerException() {
        List<CotizacionRepository> listadoCotizacionRepository = null;
        CotizacionService cotizacionService = new CotizacionService();
        cotizacionService.setCotizacionRepositoryListado(listadoCotizacionRepository);
        cotizacionService.obtenerListadoCotizacion();
    }

    @Test
    public void obtenerListadoCotizacion_ArgumentoValidoArrayCotizacionRepository_listadoObtenido() {
        List<CotizacionRepository> listadoCotizacionRepository = new ArrayList();
        listadoCotizacionRepository.add(new CoinDeskCotizacionRepository());
        CotizacionService cotizacionService = new CotizacionService();
        cotizacionService.setCotizacionRepositoryListado(listadoCotizacionRepository);
        assertNotNull(cotizacionService.obtenerListadoCotizacion());
    }
}