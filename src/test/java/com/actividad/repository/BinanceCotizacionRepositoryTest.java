package com.actividad.repository;

import com.actividad.cotizacionweb.repository.BinanceCotizacionRepository;
import jodd.http.HttpException;
import org.json.JSONException;
import org.junit.Test;
import static org.junit.Assert.*;

public class BinanceCotizacionRepositoryTest {

    public BinanceCotizacionRepositoryTest() {
    }

    @Test(expected = HttpException.class)
    public void obtenerCotizacion_sinUrl_httpException() {
        BinanceCotizacionRepository binanceCotizacionRepository = new BinanceCotizacionRepository();
        binanceCotizacionRepository.setUrl("");
        binanceCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = JSONException.class)
    public void obtenerCotizacion_urlInvalida_JSONException() {
        BinanceCotizacionRepository binanceCotizacionRepository = new BinanceCotizacionRepository();
        binanceCotizacionRepository.setUrl("https://api.coindesk.com/v1/bpi/currentprice.xml");
        binanceCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = NullPointerException.class)
    public void obtenerCotizacion_urlNull_NullPointerException() {
        BinanceCotizacionRepository binanceCotizacionRepository = new BinanceCotizacionRepository();
        binanceCotizacionRepository.setUrl(null);
        binanceCotizacionRepository.obtenerCotizacion();
    }

    @Test
    public void obtenerCotizacion_urlValida_cotizacionObtenida() {
        BinanceCotizacionRepository binanceCotizacionRepository = new BinanceCotizacionRepository();
        assertNotNull(binanceCotizacionRepository.obtenerCotizacion());
    }
}
