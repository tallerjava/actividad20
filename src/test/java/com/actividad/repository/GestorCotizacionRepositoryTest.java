package com.actividad.repository;

import com.actividad.cotizacionweb.cotizacion.Cotizacion;
import com.actividad.cotizacionweb.repository.GestorCotizacionRepository;
import java.sql.SQLException;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

public class GestorCotizacionRepositoryTest {
/*
    @Test(expected = SQLException.class)
    public void obtenerUltimaCotizacion_urlInvalida_SQLException() throws SQLException {
        GestorCotizacionRepository gestorCotizacionRepository = new GestorCotizacionRepository();
        gestorCotizacionRepository.setUrlLocal("https://api.coindesk.com/v1/bpi/currentprice.xml");
        gestorCotizacionRepository.obtenerUltimaCotizacion("CoinDesk");
    }

    @Test(expected = SQLException.class)
    public void obtenerUltimaCotizacion_urlNull_SQLException() throws SQLException {
        GestorCotizacionRepository gestorCotizacionRepository = new GestorCotizacionRepository();
        gestorCotizacionRepository.setUrlLocal(null);
        gestorCotizacionRepository.obtenerUltimaCotizacion("CoinDesk");
    }

    @Test(expected = SQLException.class)
    public void obtenerUltimaCotizacion_usuarioInvalido_SQLException() throws SQLException {
        GestorCotizacionRepository gestorCotizacionRepository = new GestorCotizacionRepository();
        gestorCotizacionRepository.setUsuario("User401");
        gestorCotizacionRepository.obtenerUltimaCotizacion("CoinDesk");
    }

    @Test(expected = SQLException.class)
    public void obtenerUltimaCotizacion_passwordInvalido_SQLException() throws SQLException {
        GestorCotizacionRepository gestorCotizacionRepository = new GestorCotizacionRepository();
        gestorCotizacionRepository.setPassword("caperusita401");
        gestorCotizacionRepository.obtenerUltimaCotizacion("CoinDesk");
    }

    @Test
    public void obtenerUltimaCotizacion_condicionesValidas_ultimaCotizacionObtenida() throws SQLException {
        GestorCotizacionRepository gestorCotizacionRepository = new GestorCotizacionRepository();
        assertNotNull(gestorCotizacionRepository.obtenerUltimaCotizacion("CoinDesk"));
    }

    @Test(expected = SQLException.class)
    public void guardarCotizacion_urlInvalida_SQLException() throws SQLException {
        Cotizacion cotizacion = new Cotizacion("CoinDesk", new Date(), "EUR", 1000.10);
        GestorCotizacionRepository gestorCotizacionRepository = new GestorCotizacionRepository();
        gestorCotizacionRepository.setUrlLocal("https://api.coindesk.com/v1/bpi/currentprice.xml");
        gestorCotizacionRepository.guardarCotizacion(cotizacion);
    }

    @Test(expected = SQLException.class)
    public void guardarCotizacion_urlNull_SQLException() throws SQLException {
        Cotizacion cotizacion = new Cotizacion("CoinDesk", new Date(), "EUR", 1000.10f);
        GestorCotizacionRepository gestorCotizacionRepository = new GestorCotizacionRepository();
        gestorCotizacionRepository.setUrlLocal(null);
        gestorCotizacionRepository.guardarCotizacion(cotizacion);
    }

    @Test(expected = SQLException.class)
    public void guardarCotizacion_usuarioInvalido_SQLException() throws SQLException {
        Cotizacion cotizacion = new Cotizacion("CoinDesk", new Date(), "EUR", 1000.10f);
        GestorCotizacionRepository gestorCotizacionRepository = new GestorCotizacionRepository();
        gestorCotizacionRepository.setUsuario("User401");
        gestorCotizacionRepository.guardarCotizacion(cotizacion);
    }

    @Test(expected = SQLException.class)
    public void guardarCotizacion_passwordInvalido_SQLException() throws SQLException {
        Cotizacion cotizacion = new Cotizacion("CoinDesk", new Date(), "EUR", 1000.10f);
        GestorCotizacionRepository gestorCotizacionRepository = new GestorCotizacionRepository();
        gestorCotizacionRepository.setPassword("caperusita401");
        gestorCotizacionRepository.guardarCotizacion(cotizacion);
    }

    @Test
    public void guardarCotizacion_condicionesValidas_cotizacionAlmacenada() throws SQLException {
        Cotizacion cotizacion = new Cotizacion("CoinDesk", new Date(), "EUR", 1000.10f);
        GestorCotizacionRepository gestorCotizacionRepository = new GestorCotizacionRepository();
        gestorCotizacionRepository.guardarCotizacion(cotizacion);
        Cotizacion ultimaCotizacion = gestorCotizacionRepository.obtenerUltimaCotizacion("CoinDesk");
        String resultadoObtenido = ultimaCotizacion.toString();
        String resultadoEsperado = cotizacion.toString();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }*/
}