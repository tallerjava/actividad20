package com.actividad.repository;

import com.actividad.cotizacionweb.repository.FinanSurCotizacionRepository;
import org.junit.Test;
import static org.junit.Assert.*;

public class FinanSurCotizacionRepositoryTest {

    public FinanSurCotizacionRepositoryTest() {
    }

    @Test
    public void obtenerCotizacion_valoresValidos_cotizacionObtenida() {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        assertNotNull(finanSurCotizacionRepository.obtenerCotizacion());
    }
/*
    @Test(expected = NullPointerException.class)
    public void obtenerCotizacion_urlInvalida_NullPointerException() {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        finanSurCotizacionRepository.setUrlLocal("jdbc:mysql://localhost:3306/OtraBaseLocal");
        finanSurCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = NullPointerException.class)
    public void obtenerCotizacion_urlNull_NullPointerException() {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        finanSurCotizacionRepository.setUrlLocal(null);
        finanSurCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = NullPointerException.class)
    public void obtenerCotizacion_usuarioInvalido_NullPointerException() {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        finanSurCotizacionRepository.setUsuario("otroUser");
        finanSurCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = NullPointerException.class)
    public void obtenerCotizacion_passwordInvalido_NullPointerException() {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        finanSurCotizacionRepository.setPassword("nuevaContr4");
        finanSurCotizacionRepository.obtenerCotizacion();
    }*/
}