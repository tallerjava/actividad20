package com.actividad.cotizacionweb.cotizacion;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cotizacion_historico")
public class Cotizacion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String proveedor;
    private Date fecha;
    private String moneda;
    private double precio;

    public Cotizacion() {
    }

    public Cotizacion(String nombreProveedor, Date fecha, String moneda, double precio) {
        this.proveedor = nombreProveedor;
        this.fecha = fecha;
        this.moneda = moneda;
        this.precio = precio;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreProveedor() {
        return proveedor;
    }

    public Date getFecha() {
        return fecha;
    }

    public String getMoneda() {
        return moneda;
    }

    public double getPrecio() {
        return precio;
    }

    @Override
    public String toString() {
        DecimalFormat formatoDecimal = new DecimalFormat(".##");
        String formatFecha = new SimpleDateFormat("d-M-yyyy HH:mm").format(fecha);
        return proveedor + ":\t" + formatFecha + " / " + moneda + " " + formatoDecimal.format(precio);
    }
}
