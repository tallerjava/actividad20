package com.actividad.cotizacionweb.service;

import com.actividad.cotizacionweb.cotizacion.Cotizacion;
import com.actividad.cotizacionweb.repository.CotizacionRepository;
import com.actividad.cotizacionweb.repository.GestorCotizacionRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CotizacionService {

    @Autowired
    private List<CotizacionRepository> cotizacionRepositoryListado;

    @Autowired
    private GestorCotizacionRepository gestorCotizacionRepository;

    public CotizacionService() {
    }

    public void setCotizacionRepositoryListado(List<CotizacionRepository> cotizacionRepositoryListado) {
        this.cotizacionRepositoryListado = cotizacionRepositoryListado;
    }

    public List<Cotizacion> obtenerListadoCotizacion() {

        List<Cotizacion> cotizacionListado = new ArrayList();
        for (CotizacionRepository cotizacionRepository : cotizacionRepositoryListado) {
            String nombreProveedor = cotizacionRepository.getNombreProveedor();
            Cotizacion cotizacion;
            try {
                cotizacion = cotizacionRepository.obtenerCotizacion();
            } catch (Exception obtenerCotizacionException) {
                cotizacion = null;
            }
            if (cotizacion != null) {
                cotizacionListado.add(cotizacion);
                try {
                    gestorCotizacionRepository.guardarCotizacion(cotizacion);
                } catch (Exception guardarCotizacionException) {
                    guardarCotizacionException.printStackTrace();
                }
            } else {
                try {
                    cotizacionListado.add(gestorCotizacionRepository.obtenerUltimaCotizacion(nombreProveedor));
                } catch (Exception obtenerUltimaCotizacionException) {
                    cotizacionListado.add(new Cotizacion(nombreProveedor, null, null, -1));
                }
            }
        }
        return cotizacionListado;
    }
}
