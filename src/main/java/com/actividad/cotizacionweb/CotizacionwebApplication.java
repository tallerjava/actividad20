package com.actividad.cotizacionweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CotizacionwebApplication {

    public static void main(String[] args) {
        SpringApplication.run(CotizacionwebApplication.class, args);
    }
}
