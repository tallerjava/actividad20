
package com.actividad.cotizacionweb.controller;

import com.actividad.cotizacionweb.cotizacion.Cotizacion;
import com.actividad.cotizacionweb.service.CotizacionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CotizacionAppController {
    
    @Autowired
    private CotizacionService cotizacionService;

    @RequestMapping(value = "/ListadoCotizaciones")
    public String ListadoCotizaciones(Model modelo) {
        List<Cotizacion> cotizacionListado = cotizacionService.obtenerListadoCotizacion();
        modelo.addAttribute("cotizacionListado", cotizacionListado);
        return "ListadoCotizaciones";
    }
}
