package com.actividad.cotizacionweb.repository;

import com.actividad.cotizacionweb.cotizacion.Cotizacion;
import java.sql.SQLException;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GestorCotizacionRepository {

    @Autowired
    private CotizacionRepositoryInterface cotizacionRepositoryInterface;

    public GestorCotizacionRepository() {
    }

    public Cotizacion obtenerUltimaCotizacion(String nombreProveedor) {
        Cotizacion cotizacion = null;
        try {
            cotizacion = cotizacionRepositoryInterface.findTop1ByProveedorAndFechaBeforeOrderByFechaDesc(nombreProveedor, new Date());
        } catch (Exception obtenerUltimaCotizacionException) {
            obtenerUltimaCotizacionException.printStackTrace();
        }
        if (cotizacion == null) {
            throw new NullPointerException("No se ha podido realizar la operacion. Cotizacion invalida.");
        } else {
            return cotizacion;
        }
    }

    public void guardarCotizacion(Cotizacion cotizacion) throws SQLException {
        try {
            cotizacionRepositoryInterface.save(cotizacion);
        } catch (Exception guardarCotizacionException) {
            throw new SQLException("guardarCotizacionException: Tiene problemas para guardar la cotizacion.");
        }
    }
}
