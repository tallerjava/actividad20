package com.actividad.cotizacionweb.repository;

import com.actividad.cotizacionweb.cotizacion.Cotizacion;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FinanSurCotizacionRepository extends CotizacionRepository {

    @Autowired
    private CotizacionRepositoryInterface cotizacionRepositoryInterface;
    private String nombreProveedor = "FinanSur";
    private String moneda = "USD";

    @Override
    public String getNombreProveedor() {
        return nombreProveedor;
    }

    @Override
    public String getMoneda() {
        return moneda;
    }

    @Override
    public Cotizacion obtenerCotizacion() {

        Cotizacion cotizacion = null;
        try {
            cotizacion = cotizacionRepositoryInterface.findTop1ByProveedorAndFechaBeforeOrderByFechaDesc(nombreProveedor, new Date());
        } catch (Exception obtenerCotizacionException) {
            obtenerCotizacionException.printStackTrace();
        }
        return cotizacion;
    }
}
