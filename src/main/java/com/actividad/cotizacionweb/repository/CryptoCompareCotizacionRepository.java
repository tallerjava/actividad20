
package com.actividad.cotizacionweb.repository;

import com.actividad.cotizacionweb.cotizacion.Cotizacion;
import java.util.Date;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class CryptoCompareCotizacionRepository extends CotizacionRepository {

    private String url = "https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD";
    private String nombreProveedor = "CryptoCompare";
    private String moneda = "USD";

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String getNombreProveedor() {
        return nombreProveedor;
    }

    @Override
    public String getMoneda() {
        return moneda;
    }

    @Override
    public Cotizacion obtenerCotizacion() {
        HttpResponse response = HttpRequest.get(url).send();
        JSONObject body = new JSONObject(response.body());
        Date fecha = new Date();
        double precio = body.getDouble(moneda);
        Cotizacion cotizacion = new Cotizacion(nombreProveedor, fecha, moneda, precio);
        return cotizacion;
    }
}