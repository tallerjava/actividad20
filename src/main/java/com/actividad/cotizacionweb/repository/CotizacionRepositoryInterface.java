package com.actividad.cotizacionweb.repository;

import com.actividad.cotizacionweb.cotizacion.Cotizacion;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CotizacionRepositoryInterface extends JpaRepository<Cotizacion, Long> {

    Cotizacion findTop1ByProveedorAndFechaBeforeOrderByFechaDesc(String proveedor, Date fecha);
}
